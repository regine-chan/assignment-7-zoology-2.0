﻿using System;
using System.Collections.Generic;

namespace Assignment_5
{
    class Program
    {
        static void Main(string[] args)
        {
            // initilizes some animals
            IslandHorse Horse1 = new IslandHorse(new HashSet<string>() { "gray", "black" }, 190, "Horsey");
            NormalHorse Horse2 = new NormalHorse("Reginald", new HashSet<string>() { "brown" }, 120, "Unknow");
            Animal Monkey = new Animal(50, new HashSet<string>() { "brown", "yellow", "red", "blue" });
            Animal Cat = new Animal(20, new HashSet<string>() { "black" });

            // A collection to store the animals in
            HashSet<Animal> PetZoo = new HashSet<Animal>();

            // adds the animals to the collction
            PetZoo.Add(Horse1);
            PetZoo.Add(Horse2);
            PetZoo.Add(Monkey);
            PetZoo.Add(Cat);

            // Iterates through the animals in the collection and prints their type, color and height
            foreach(Animal animal in PetZoo)
            {
                Console.WriteLine($"The {animal.GetType()} has these colors: ");
                foreach(string color in animal.Colors)
                {
                    Console.WriteLine(color);
                }

                Console.WriteLine($"\nThe animal is {animal.Height} cm tall!");

                // The animal jumps
                animal.Jump();
            }

            // The horse neighs
            Console.WriteLine("Some animal specific action: ");
            Horse1.Neigh(); 
        }
    }
}
