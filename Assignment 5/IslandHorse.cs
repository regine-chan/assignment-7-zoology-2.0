﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Assignment_5
{
    public class IslandHorse : Horse
    {
        public Saddle HorseSaddle { get; set; }

        public IslandHorse(HashSet<string> Colors, int Height, string Name) : base(Height, Colors, Name)
        {

        }

        override public void Neigh()
        {
            Console.WriteLine("This horse is apparently mute");
        }
    }
}
