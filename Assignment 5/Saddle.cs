﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Assignment_5
{
   
    public class Saddle
    {
        public string Color { get; set; }

        public Saddle(string Color)
        {
            this.Color = Color;
        }

        public void Mount()
        {
            Console.WriteLine("Mounted saddle");
        }
    }
}
