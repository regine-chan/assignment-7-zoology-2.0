﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5
{
    class NormalHorse : Horse
    {
        public string OwnerName { get; set; }

        public NormalHorse(string OwnerName, HashSet<string> Colors, int Height, string Name) : base(Height, Colors, Name)
        {
            this.OwnerName = OwnerName;
        }

        public void Move()
        {
            Console.WriteLine("The horse is moving");
        }
    }
}
